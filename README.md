# Implementacion algoritmo mergeSort en Elixir

[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://spacemacs.org)

- Toma los datos de un archivo **elements.dat***
- Los pone en un arreglo con los datos ya convertidos
- Envia cada una de las lineas y las ordena de Mayor a Menor
- Uso de recursividad
- Cambios minimos para que se ordene de Menor a Mayor

## Ejecucion

1. Se Carga iex
2. Se ejecuta
```elixir
Ejecucion.main()
```
3. Se obtienen resultados similares a:

![Ejecucion Arreglo no Ordenado](img/algoritmo_merge.png "Ejecucion Arreglo no Ordenado")

Cuando se ejecute el Merge, la organizacion resulta asi

![Ejecucion Arreglo Ordenado](img/algoritmo_merge.png "Ejecucion Arreglo Ordenado")
