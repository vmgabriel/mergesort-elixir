defmodule FileManager do
  def read_file(file) do
    if File.exists?(file) do
      File.read!(file)
    end
  end

  def list_line(file) do
    list = convert_number_line(String.split(read_file(file), "\n"))
    # DIvido el string en partes pequeñas hsata \n
  end

  def convert_number_line([]), do: []
  def convert_number_line(list) do
    [ head | tail] = list
    # Convierto a numeros cada uno de los elementos
    [ convert_elems_to_number(String.split(head, " ")) | convert_number_line(tail) ]
  end

  def convert_elems_to_number([]), do: []
  def convert_elems_to_number(elems) do
    [ head | tail ] = elems
    { val , _ } = Integer.parse(head)
    [ val | convert_elems_to_number(tail) ]
  end
end

defmodule Lista do
  def ordenarMerge([]), do: []
  # En Caso que este Vacio
  def ordenarMerge([x]), do: [x]
  # En Caso de Encontrar solo 1 elemento
  def ordenarMerge(lista) do
    lista1 = Enum.take_every(lista, 2)
    lista2 = Enum.drop_every(lista, 2)
    # Toma la mitad
    merge(ordenarMerge(lista1), ordenarMerge(lista2), [])
  end

  def merge(lista1, [], []), do: lista1 # Guarda cuando r este vacio
  def merge([], lista2, []), do: lista2
  def merge(lista1, [], r), do: r ++ lista1 # Cuando se quieran unir
  def merge([], lista2, r), do: r ++ lista2
  def merge([ l1h | l1t ] = lista1, [ l2h | l2t ] = lista2, r) do
    # Tomo Cabeza y cola de cada una de las listas
    cond do
      l1h > l2h -> merge(l1t, lista2, r ++ [l1h]) # En caso que sea mayor
      true -> merge(lista1, l2t, r ++ [l2h]) # En cualquier otro caso
    end
  end
end

defmodule Ejecucion do
  def main do
    IEx.configure(inspect: [charlists: :as_lists])
    file = "elements.dat"
    lista_elementos = FileManager.list_line(file)

    IO.puts("---- Elementos No Ordenados ----")
    IO.inspect(lista_elementos, charlists: :as_lists)
    lista_elementos = Enum.map(lista_elementos, fn(x) -> Lista.ordenarMerge(x) end)
    IO.puts("\n---- Elementos Debidamente Organizados ----")
    IO.inspect(lista_elementos, charlists: :as_lists)
    :ok
  end
end
